# TASK MANAGER

## DEVELOPER INFO

name: Sergey Mochalkin

email: smochalkin@gmail.com

## HARDWARE

CPU: i7

RAM: 32Gb

SSD: 476Gb

## SOFTWARE

OS: Windows 10 (1909)

JDK: 15.0.1

## APPLICATION BUILD

```
mvn clean install
```

## APPLICATION RUN

```
java -jar ./task-manager-1.10.0.jar
```
