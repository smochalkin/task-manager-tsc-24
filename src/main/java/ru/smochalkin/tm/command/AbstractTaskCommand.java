package ru.smochalkin.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.exception.entity.TaskNotFoundException;
import ru.smochalkin.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().getDisplayName());
        System.out.println("Project: " + task.getProjectId());
    }

}
