package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectCompleteByNameCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-complete-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Complete project by name.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.print("Enter name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findByName(userId, name);
        serviceLocator.getProjectService().updateStatusByName(userId, name, Status.COMPLETED);
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
