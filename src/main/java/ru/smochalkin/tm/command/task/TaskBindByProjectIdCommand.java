package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.entity.ProjectNotFoundException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskBindByProjectIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "task-bind-by-project-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Bind task to project by id.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        serviceLocator.getAuthService().getUserId();
        System.out.print("Enter project id: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectTaskService().isNotProjectId(projectId)) {
            throw new ProjectNotFoundException();
        }
        System.out.print("Enter task id: ");
        @NotNull final String taskId = TerminalUtil.nextLine();
        serviceLocator.getProjectTaskService().isTaskId(taskId);
        serviceLocator.getProjectTaskService().bindTaskByProjectId(projectId, taskId);
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
