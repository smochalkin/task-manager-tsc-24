package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-complete-by-index";
    }

    @Override
    @NotNull
    public String description() {
        return "Complete task by index.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.print("Enter index: ");
        @NotNull Integer index = TerminalUtil.nextInt();
        index--;
        serviceLocator.getTaskService().findByIndex(userId, index);
        serviceLocator.getTaskService().updateStatusByIndex(userId, index, Status.COMPLETED);
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
