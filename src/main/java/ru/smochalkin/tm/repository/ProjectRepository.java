package ru.smochalkin.tm.repository;

import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.model.Project;

public final class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {
}
