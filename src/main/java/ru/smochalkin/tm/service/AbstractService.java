package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IRepository;
import ru.smochalkin.tm.api.IService;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    @NotNull
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        return repository.findAll(comparator);
    }

    @Override
    @Nullable
    public E findById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    @NotNull
    public E removeById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public void add(@Nullable final E entity) {
        if (entity == null) throw new EmptyObjectException();
        repository.add(entity);
    }

    @Override
    @NotNull
    public E remove(@Nullable final E entity) {
        if (entity == null) throw new EmptyObjectException();
        return repository.remove(entity);
    }

    @Override
    public int getCount() {
        return repository.getCount();
    }

}
