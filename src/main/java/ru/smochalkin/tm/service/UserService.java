package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.api.service.IUserService;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.*;
import ru.smochalkin.tm.exception.entity.UserNotFoundException;
import ru.smochalkin.tm.exception.system.LoginExistsException;
import ru.smochalkin.tm.model.User;
import ru.smochalkin.tm.util.HashUtil;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    @NotNull
    public User findByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        userRepository.removeByLogin(login);
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isLogin(login)) throw new LoginExistsException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        userRepository.add(user);
        return user;
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isLogin(login)) throw new LoginExistsException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setEmail(email);
        userRepository.add(user);
        return user;
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isLogin(login)) throw new LoginExistsException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);
        userRepository.add(user);
        return user;
    }

    @Override
    public void setPassword(@Nullable final String userId, @Nullable final String password) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @Nullable final User user = findById(userId);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
    }

    @Override
    public void updateById(@Nullable final String id,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName) {
        if (isEmpty(id)) throw new EmptyIdException();
        @Nullable final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
    }

    @Override
    public boolean isLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        return userRepository.isLogin(login);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @NotNull final User user = userRepository.findByLogin(login);
        user.setLock(true);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @NotNull final User user = userRepository.findByLogin(login);
        user.setLock(false);
    }

}

